package com.moderngravel.umbrellaedit.util;

public class DijkstraVertex {
	private int x, y;
	
	private int value;
	
	private DijkstraVertex last;

	public DijkstraVertex(int x, int y, int value) {
		this.x = x;
		this.y = y;
		this.value = value;
	}

	public DijkstraVertex getLast() {
		return last;
	}

	public void setLast(DijkstraVertex last) {
		this.last = last;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getValue(){
		return value;
	}
}
