package com.moderngravel.umbrellaedit.util;

import java.awt.image.BufferedImage;
import java.util.Map;

public class HistoryItem {

	private BufferedImage image;
	private Map<String, String> required;
	private Map<String, Object> supplied;
	private String type;

	public HistoryItem(String type, BufferedImage currentImage, Map<String, String> required, Map<String, Object> supplied) {
		this.image = currentImage;
		this.required = required;
		this.supplied = supplied;
		this.type = type;
	}

	public Map<String, String> getRequired() {
		return required;
	}

	public Map<String, Object> getSupplied() {
		return supplied;
	}

	public BufferedImage getImage() {
		return image;
	}
	
	public String getType(){
		return type;
	}

}
