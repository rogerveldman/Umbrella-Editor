package com.moderngravel.umbrellaedit.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.moderngravel.umbrellaedit.effect.BellCurveBlurFilter;
import com.moderngravel.umbrellaedit.effect.DitherQuantizeFilter;
import com.moderngravel.umbrellaedit.effect.GrayScaleFilter;
import com.moderngravel.umbrellaedit.effect.HSBFilter;
import com.moderngravel.umbrellaedit.effect.InvertFilter;
import com.moderngravel.umbrellaedit.effect.MeanBlurFilter;
import com.moderngravel.umbrellaedit.effect.SeamCarvingEffect;
import com.moderngravel.umbrellaedit.effect.SobelEffect;
import com.moderngravel.umbrellaedit.framework.Effect;
import com.moderngravel.umbrellaedit.util.HistoryItem;

public class Window extends JFrame {
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private BufferedImage currentImage;
	private Stack<HistoryItem> history;
	private volatile int displayHeight;
	private Map<String, Effect> effects;

	public Window(File file) throws Exception {
		super();

		try {
			currentImage = ImageIO.read(file);
		} catch (IOException e) {
			throw new Exception();
		}

		effects = new TreeMap<String, Effect>();

		displayHeight = currentImage.getHeight();

		history = new Stack<HistoryItem>();

		setTitle("umbrella");

		label = new JLabel(new ImageIcon(currentImage));

		JPanel jp = new JPanel(new BorderLayout());

		jp.add(label, BorderLayout.CENTER);

		add(jp);

		addEffect(new MeanBlurFilter());
		addEffect(new BellCurveBlurFilter());
		addEffect(new GrayScaleFilter());
		addEffect(new DitherQuantizeFilter());
		addEffect(new InvertFilter());
		addEffect(new HSBFilter());
		addEffect(new SobelEffect());
		addEffect(new SeamCarvingEffect());
		// effects.add(new PatchEffect());

		JMenuBar bar = new JMenuBar();
		JMenu filtersMenu = new JMenu("Filters");
		JMenu fileMenu = new JMenu("File");

		JMenuItem save = new JMenuItem("Export Image...");

		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser saveChooser = new JFileChooser();
				if (saveChooser.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
					return;
				}

				try {
					String filename = saveChooser.getSelectedFile().getName();
					String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
					System.out.println(extension);
					ImageIO.write(currentImage, extension, saveChooser.getSelectedFile());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Could not save image");
				}
			}
		});

		ImageIcon umbrellaIcon = new ImageIcon(getClass().getClassLoader().getResource("./umbrella.png"));
		this.setIconImage(umbrellaIcon.getImage());

		fileMenu.add(save);

		JMenuItem quitItem = new JMenuItem("Quit");

		quitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		fileMenu.add(quitItem);

		JMenu editMenu = new JMenu("Edit");

		JMenuItem undoButton = new JMenuItem("Undo");
		undoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				undo();
			}
		});

		editMenu.add(undoButton);

		JMenuItem makePatch = new JMenuItem("Create Patch...");

		makePatch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser patchChoose = new JFileChooser();
				if (patchChoose.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
					return;
				}

				PrintWriter writePatch = null;
				try {
					writePatch = new PrintWriter(patchChoose.getSelectedFile());
					writePatch.print("{");
					boolean first = true;
					for (HistoryItem hi : history) {
						if (!first) {
							writePatch.print(",");
						} else {
							first = false;
						}
						JSONObject detail = new JSONObject();
						for (String sup : hi.getSupplied().keySet()) {
							detail.put(sup, hi.getSupplied().get(sup).toString());
						}
						writePatch.print("\"" + hi.getType() + "\":" + detail);
					}
					writePatch.print("}");
				} catch (FileNotFoundException e1) {
					JOptionPane.showMessageDialog(null, "Could not save file");
				}
				System.out.println("wrote patch to " + patchChoose.getSelectedFile());
				writePatch.close();
			}
		});

		editMenu.add(makePatch);

		JMenuItem applyPatch = new JMenuItem("Apply Patch...");

		applyPatch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser patchOpenChoose = new JFileChooser();
				if (patchOpenChoose.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				try {
					applyPatchFile(patchOpenChoose.getSelectedFile().getPath());
				} catch (JSONException e) {
					if (!e.getMessage().contains("Duplicate")) {
						JOptionPane.showMessageDialog(null, "JSON Error: Malformed");
					} else {
						JOptionPane.showMessageDialog(null, "JSON Error: Cannot load same effect twice");
					}
				}
			}
		});

		editMenu.add(applyPatch);

		JMenu viewMenu = new JMenu("View");

		JMenuItem zoomIn = new JMenuItem("Zoom In");
		zoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayHeight = (int) (1.5 * displayHeight);
				redrawImage();
			}
		});

		viewMenu.add(zoomIn);

		JMenuItem zoomOut = new JMenuItem("Zoom Out");
		zoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayHeight = (int) ((2.0 / 3.0) * displayHeight);
				redrawImage();
			}
		});

		viewMenu.add(zoomOut);

		for (double i = 25; i <= 800; i *= 2) {
			JMenuItem zoom = new JMenuItem("Zoom " + i + "%");
			Double percentPass = (double) i;
			zoom.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					final double percent = percentPass;
					displayHeight = (int) ((percent / 100.0) * currentImage.getHeight());
					redrawImage();
				}
			});

			viewMenu.add(zoom);
		}

		bar.add(fileMenu);
		bar.add(editMenu);
		bar.add(filtersMenu);
		bar.add(viewMenu);
		setJMenuBar(bar);

		for (final Effect eff : effects.values()) {
			final JPanel options = new JPanel();
			options.setLayout(new BoxLayout(options, BoxLayout.Y_AXIS));
			Map<String, String> map = eff.getParams();

			final Map<String, Component> componentMap = new TreeMap<String, Component>();

			if (map.keySet().isEmpty()) {
				options.add(new JLabel(" "), "span, grow");
			}

			for (String param : map.keySet()) {
				if (map.get(param).equals("percent")) {
					JPanel lilPanel = new JPanel(new BorderLayout());
					String labelText = param.substring(0, 1).toUpperCase() + param.substring(1, param.length());
					JLabel lilLabelName = new JLabel(labelText + ":");
					lilLabelName.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
					lilPanel.add(lilLabelName, BorderLayout.WEST);
					JSlider slider = new JSlider(0, 100);
					slider.setValue(50);
					slider.setMajorTickSpacing(20);
					slider.setMinorTickSpacing(10);
					slider.setPaintTicks(true);
					slider.setPaintLabels(true);
					lilPanel.add(slider, BorderLayout.CENTER);
					componentMap.put(param, slider);
					options.add(lilPanel, BorderLayout.EAST);
				} else if (map.get(param).equals("colorlist")) {
					options.add(new JLabel("Colors (one per line, hex):"));
					JTextArea colors = new JTextArea();
					colors.setText("#FF0000\n#FFFF00\n#00FFFF\n#00FF00\n#0000FF\n#0000FF");
					JScrollPane colorPane = new JScrollPane(colors);
					colorPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
					options.add(colorPane);
					componentMap.put(param, colors);
					JButton randomButton = new JButton("Randomize from ColourLovers.com");
					JLabel colorsPreview = new JLabel();

					randomButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							StringBuilder result = new StringBuilder();
							try {
								colors.setText("#FF0000\n#FFFF00");
								URL url = new URL("http://www.colourlovers.com/api/palettes/random?format=json");
								HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
								conn.setRequestMethod("GET");

								BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
								String line;
								while ((line = rd.readLine()) != null) {
									result.append(line);
								}
								rd.close();
							} catch (IOException e1) {
								JOptionPane.showMessageDialog(null, "Could not connect");
							}
							options.add(colorsPreview);

							if (!result.toString().isEmpty()) {
								JSONObject obj = new JSONArray(result.toString()).getJSONObject(0);
								StringBuilder colorListSb = new StringBuilder();
								JSONArray colorsJsonArray = obj.getJSONArray("colors");
								for (int i = 0; i < colorsJsonArray.length(); i++) {
									colorListSb.append("#"+colorsJsonArray.get(i));
									if (i<colorsJsonArray.length()-1) {
										colorListSb.append("\r\n");
									}
								}
								colorsPreview.setText(obj.getString("title") +" by " + obj.getString("userName"));
								colors.setText(colorListSb.toString());
								colors.setSize(10,colorsJsonArray.length());
							}
						}
					});
					options.add(randomButton);
				} else if (map.get(param).equals("boolean")) {
					JCheckBox cb = new JCheckBox();
					JPanel lilPanel = new JPanel(new BorderLayout());
					String labelText = param.substring(0, 1).toUpperCase() + param.substring(1, param.length());
					JLabel lilLabelName = new JLabel(labelText + ":");
					lilLabelName.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
					lilPanel.add(lilLabelName, BorderLayout.WEST);
					lilPanel.add(cb, BorderLayout.CENTER);
					options.add(lilPanel);
					componentMap.put(param, cb);
				}

			}

			JMenuItem filterItem = new JMenuItem(eff.getName());
			if (!eff.getParams().isEmpty()) {
				filterItem.setText(filterItem.getText() + "...");
			}

			options.add(new JLabel(" "), "span, grow");

			filterItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					int confirm = -1;
					if (!eff.getParams().isEmpty()) {
						confirm = JOptionPane.showConfirmDialog(null, options, eff.getName(),
								JOptionPane.OK_CANCEL_OPTION);
						if (confirm != JOptionPane.OK_OPTION) {
							return;
						}
					}

					Map<String, String> req = eff.getParams();

					Map<String, Object> params = new TreeMap<String, Object>();
					for (String reqString : req.keySet()) {
						if (req.get(reqString).equals("percent")) {
							JSlider slider = (JSlider) componentMap.get(reqString);
							params.put(reqString, slider.getValue());
						} else if (req.get(reqString).equals("colorlist")) {
							JTextArea slider = (JTextArea) componentMap.get(reqString);
							String sliderText = slider.getText().replaceAll("\n", ",").replaceAll("\r", "");
							params.put(reqString, sliderText);
						} else if (req.get(reqString).equals("boolean")) {
							JCheckBox cb = (JCheckBox) componentMap.get(reqString);
							params.put(reqString, cb.isSelected());
						}
					}

					applyEffect(eff, params);
				}
			});

			filtersMenu.add(filterItem);
		}

		pack();

		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Map<String, Object> map = new TreeMap<String, Object>();
		map.put("size", 5);

		int twoThirdsHeight = (int) (Toolkit.getDefaultToolkit().getScreenSize().height * 2.0 / 3.0);

		if (currentImage.getHeight() > twoThirdsHeight) {
			displayHeight = twoThirdsHeight;
		}
		redrawImage();
	}

	private void addEffect(Effect effect) {
		effects.put(effect.getName(), effect);
	}

	public void undo() {
		if (!history.isEmpty()) {
			HistoryItem his = history.pop();
			currentImage = his.getImage();
			redrawImage();
		}
	}

	public void applyEffect(Effect effect, Map<String, Object> params) {
		history.push(new HistoryItem(effect.getName(), currentImage, effect.getParams(), params));
		effect.init(params);
		ColorModel copycm = currentImage.getColorModel();
		boolean copypremult = currentImage.isAlphaPremultiplied();
		WritableRaster copyraster = currentImage.copyData(null);
		BufferedImage copy = new BufferedImage(copycm, copyraster, copypremult, null);
		long time = System.currentTimeMillis();
		JFrame progressFrame = new JFrame("Computing...");
		progressFrame.setSize(200, 0);
		progressFrame.setVisible(true);
		progressFrame.setLocationRelativeTo(null);
		currentImage = effect.getProcessedImage(copy, progressFrame);
		progressFrame.dispose();
		System.out.println(effect.getName() + ": " + (System.currentTimeMillis() - time) + "ms");

		redrawImage();
	}

	public void applyPatchFile(String url) throws JSONException {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(url))) {
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONObject obj = new JSONObject(sb.toString());
		for (String type : obj.keySet()) {
			JSONObject paramsObj = obj.getJSONObject(type);
			Map<String, Object> params = new TreeMap<>();
			for (String paramKey : paramsObj.keySet()) {
				params.put(paramKey, paramsObj.get(paramKey));
			}
			applyEffect(effects.get(type), params);
		}

	}

	private void redrawImage() {
		int displayWidth = (int) (displayHeight * (double) currentImage.getHeight() / (double) currentImage.getWidth());
		ImageIcon icon = new ImageIcon(currentImage.getScaledInstance(displayHeight, displayWidth, Image.SCALE_SMOOTH));
		label.setIcon(icon);
	}
}
